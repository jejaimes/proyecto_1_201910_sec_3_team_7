package model.data_structures;

import junit.framework.TestCase;
import model.data_structures.ArregloDinamico;
import model.data_structures.Stack;

public class TestStack extends TestCase{

	private Stack<String> prueba;

	public void setUpScenario0()
	{
		ArregloDinamico<String> ar = new ArregloDinamico<String>(11000);
		prueba = new Stack<String>(ar);
	}

	public void setUpScenario1()
	{
		ArregloDinamico<String> ar = new ArregloDinamico<String>(11000);
		for(int i = 0; i < 10000; i++)
		{
			ar.agregar("nombre"+i);
		}
		prueba = new Stack<String>(ar);
	}

	public void testIsEmpty()
	{
		setUpScenario0();
		assertEquals(true, prueba.isEmpty());

		setUpScenario1();
		assertEquals(false, prueba.isEmpty());
	}

	public void testSize()
	{
		setUpScenario0();
		assertEquals(0, prueba.size());

		setUpScenario1();
		assertEquals(10000, prueba.size());
	}

	public void testPopyPush()
	{
		setUpScenario1();
		assertEquals("nombre9999", prueba.pop());
		assertEquals("nombre9998", prueba.pop());

		prueba.push("nombre10000");
		assertEquals("nombre10000", prueba.pop());
	}
}
