package model.vo;

public class VOViolationCode implements Comparable<VOViolationCode> {

	private String violationCode;
	private double FineAmit;
	private boolean accidente;
	private int numero;

	public VOViolationCode(String pViolation, double pFineamit, boolean pAccidente) {
		violationCode= pViolation;
		FineAmit=pFineamit;
		accidente=pAccidente;
		numero =1;
	}
	public String getViolationCode() {
		return violationCode;
	}

	public boolean getAccident() {
		return accidente;
	}


	public double getAvgFineAmt() {
		return FineAmit/numero;
	}

	public void registrarNueva(double pFineAmit)
	{
		FineAmit+=pFineAmit;
		numero++;
	}

	@Override
	public int compareTo(VOViolationCode o) {
		
		return violationCode.compareTo(o.getViolationCode());
		
	}

}
