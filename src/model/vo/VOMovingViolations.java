package model.vo;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations implements Comparable<VOMovingViolations> {

	private String ticketIssueDat;
	private String violationDesc;
	private int objectID;
	private double totalPaid;
	private int adressID;
	private double penalty1;
	private double penalty2;
	private int streetSegid;	
	private String violationCode;
	private double fineAmit;
	private boolean accidente;
	


	public VOMovingViolations(String pticketIssueDat,
			String pviolationDesc,int pobjectID,
			double ptotalPaid, 
			int padressID,
			double ppenalty1, double ppenalty2, int pStreetSegid,String pViolationCode, double pFineamit, boolean pAccidente)
	{
		ticketIssueDat=pticketIssueDat;
		violationDesc = pviolationDesc;
		objectID = pobjectID;
		totalPaid= ptotalPaid;
		adressID = padressID;
		penalty1 = ppenalty1;
		penalty2= ppenalty2;
		streetSegid = pStreetSegid;
		violationCode =pViolationCode;
		fineAmit = pFineamit;
		accidente = pAccidente;
		
	}




	/**
	 * @return id - Identificador unico de la infraccion
	 */
	public int objectId() {
		return objectID;
	}	

	/**
	 * @return date - Fecha cuando se puso la infraccion .
	 */
	public String getTicketIssueDate() {
		return ticketIssueDat;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagara el que recibio la infraccion en USD.
	 */
	public double getTotalPaid() {
		return totalPaid;
	}

	/**
	 * @return description - Descripcion textual de la infraccion.
	 */
	public String  getViolationDescription() {
		return violationDesc;
	}

	/**
	 * 
	 * @return adressId de la infraccion.
	 */
	public int getAdressId()
	{
		return adressID;
	}
	/**
	 * 
	 * @return penalty1 de la infraccion.
	 */
	public double getPenalty1()
	{
		return penalty1;
	}

	/**
	 * 
	 * @return penalty2 de la infraccion.
	 */
	public double getPenalty2()
	{
		return penalty2;
	}
	/**
	 * 
	 * @return streetSegID ID	del	segmento	de	la	calle.
	 */
	public int  getStreetSegid()
	{
		return streetSegid;
	}
	/**
	 * 
	 * @return String de los atributos de la infraccion.
	 */
	public String toString()
	{
		return 
				objectID + " "+ticketIssueDat + " "+violationDesc + " "+adressID + " "+
				totalPaid+ " "+
				penalty1 + " "+
				penalty2+ " "+
				streetSegid + " ";
		

	}




	@Override
	public int compareTo(VOMovingViolations o) {
		// TODO Auto-generated method stub
		return ((Integer)objectID).compareTo((Integer)o.objectId());
	}




	public String getViolationCode() {
		// TODO Auto-generated method stub
		return violationCode;
	}


	public double getFineAMT() {
		// TODO Auto-generated method stub
		return fineAmit;
	}




	public boolean getAccident() {
		// TODO Auto-generated method stub
		return accidente;
	}

}