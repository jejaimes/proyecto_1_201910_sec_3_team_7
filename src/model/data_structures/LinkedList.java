package model.data_structures;


import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<T> implements Iterable<T>{


	int size;

	NodoLista<T> primero;

	NodoLista<T>  currentElement;


	public LinkedList()
	{
		
		currentElement = primero;
		size = 0;

	}

	public Iterator<T> iterator() {

		IteradorLista<T> it = new IteradorLista<T>(primero);
		return it;
	}


	public int getSize() {

		return size;
	}


	public void add(T element) {

		NodoLista<T> nuevo = new  NodoLista<T>(element);
		if(primero== null)
		{
			primero= nuevo;

		}
		else if (primero !=null)
		{
			nuevo.cambiarSiguiente(primero);
			primero.cambiarAnterior(nuevo);
			primero= nuevo;
		}
		size++;
	}




	public void addAtK(int posicion, T element) {
		currentElement= primero;
		boolean listo = false;
		NodoLista<T> nuevo = new  NodoLista<T>(element);
		if(primero== null)
		{
			primero= nuevo;

		}

		for (int i =0; i< size&& !listo;i++)
		{
			if(currentElement != null)
			{
				if( posicion==i)
				{
					nuevo.cambiarAnterior(currentElement.darAnterior());
					nuevo.cambiarSiguiente(currentElement);
					currentElement.darAnterior().cambiarSiguiente(nuevo);
					currentElement.cambiarAnterior(nuevo);
					listo = true;
				}
				next();
			}
		}
		size++;
	}


	public T getElement( int posicion ) throws Exception 
	{
		currentElement= primero;
		if (currentElement==null)
		{
			throw new Exception ("no hay elementos");
		}

		for(int i = 0; i< size ;i++)
		{


			if(i== posicion)
			{
				return currentElement.darElemento();
			}

			next();

		}
		return null;
	}



	public T getCurrentElement() throws Exception{
		if (currentElement ==null)
		{
			throw new Exception("no hay elemnto actual");
		}
		return currentElement.darElemento();
	}



	public void delete(T element) throws Exception{
		currentElement= primero;
		if (currentElement==null)
		{
			throw new Exception ("no hay elementos");
		}


		for(int i = 0; i<= size ;i++)
		{

			if(element.equals(currentElement) )
			{
				if( currentElement .darAnterior()!=null){
					NodoLista<T> nodo = currentElement.darAnterior();
					nodo.cambiarSiguiente(currentElement.darSiguiente());
					size--;
				}

				else if( currentElement .darAnterior()==null)
				{
					primero = currentElement.darSiguiente();
					size--;
				}
			}
			next();

		}

	}

	public void deleteAtK(int posicion) {

		currentElement= primero;
		for(int i = 0; i<= size ;i++)
		{
			if(i== posicion)
			{
				if( currentElement .darAnterior()!=null){
					NodoLista<T> nodo = currentElement.darAnterior();
					nodo.cambiarSiguiente(currentElement.darSiguiente());
					size--;
				}

				else if( currentElement .darAnterior()==null)
				{
					primero = currentElement.darSiguiente();
					size--;
				}
			}
			next();

		}



	}

	public void addAtEnd(T element) {
		currentElement= primero;
		while(currentElement.darSiguiente()!=null)
		{
			next();
		}	
		
		NodoLista<T> nodo = new NodoLista<T>(element);
		currentElement.cambiarSiguiente(nodo);
		size++;
		

	}



	public void next() {
		if(currentElement!=null)
		currentElement = currentElement.darSiguiente();

	}

	public void previous() {
		currentElement = currentElement.darAnterior();

	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////             CLASE QUE FORMA LOS NODOS PARA LISTA             //////////////////////////    
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public class NodoLista<T> {

		T objeto;
		NodoLista<T> siguiente;

		NodoLista<T> anterior;

		public NodoLista(T object) 
		{
			objeto = object;
			siguiente = null;
			anterior = null;

		}

		public T darElemento()
		{
			return objeto;
		}
		public NodoLista<T> darSiguiente()
		{
			return siguiente;
		}

		public NodoLista<T> darAnterior()
		{
			return anterior;
		}

		public void	cambiarSiguiente(NodoLista<T> nodo)
		{


			siguiente = nodo;
		}
		public void	cambiarAnterior(NodoLista<T> nodo)
		{

			anterior=nodo;
		}


	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////             CLASE QUE ITERA LA LISTA ENCADENADA	            //////////////////////////    
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public class IteradorLista<T> implements Iterator<T> 
	{

		private NodoLista<T>  prox;



		public IteradorLista(NodoLista<T> first ) 
		{
			prox = first;


		}

		public boolean hasNext() {

			return prox!=null;
		}


		public T next() {

			if ( prox == null )
			{ 
				throw new NoSuchElementException("no hay proximo"); 
			}
			T elemento = prox.darElemento(); 

			prox = prox.darSiguiente(); 

			return elemento;
		}



	}



}
