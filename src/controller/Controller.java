package controller;

import java.awt.image.PixelInterleavedSampleModel;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Scanner;

import model.data_structures.ArregloDinamico;
import model.data_structures.IArregloDinamico;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VOMovingViolations;
import model.vo.VOViolationCode;

import view.MovingViolationsManagerView;

public class Controller {

	private MovingViolationsManagerView view;
	private  IQueue<VOMovingViolations> infraccionesQueue;
	private IStack<VOMovingViolations> infraccionesStack;
	private IArregloDinamico<VOViolationCode> colaViolationCode;
	private IArregloDinamico<VOMovingViolations> infracciones;
	public Controller() {
		view = new MovingViolationsManagerView();

	}

	public void run() throws Exception {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		Controller controller = new Controller();

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 0:
				view.printMessage("Ingrese el cuatrimestre (1, 2 o 3)");
				int numeroCuatrimestre = sc.nextInt();
				controller.loadMovingViolations(numeroCuatrimestre);
				break;

			case 1:
				IQueue<VOMovingViolations> isUnique =  controller.verifyObjectIDIsUnique();
				if(isUnique.isEmpty())
					view.printMessage("El objectId es unico");
				else{
					for (VOMovingViolations voMovingViolations : isUnique) {
						voMovingViolations= isUnique.dequeue();
						view.printMessage("Los siguientes objectId estan repetidos: " + voMovingViolations);
					}}

				break;

			case 2:

				view.printMessage("Ingrese la fecha con hora inicial (Ej : 28/03/2017T15:30:20)");
				LocalDateTime fechaInicialReq2A = convertirFecha_Hora_LDT(sc.next());

				view.printMessage("Ingrese la fecha con hora final (Ej : 28/03/2017T15:30:20)");
				LocalDateTime fechaFinalReq2A = convertirFecha_Hora_LDT(sc.next());

				IQueue<VOMovingViolations> resultados2 = controller.getMovingViolationsInRange(fechaInicialReq2A, fechaFinalReq2A);

				view.printMovingViolationsReq2(resultados2);

				break;

			case 3:

				view.printMessage("Ingrese el VIOLATIONCODE (Ej : T210)");
				String violationCode3 = sc.next();

				double [] promedios3 = controller.avgFineAmountByViolationCode(violationCode3);

				view.printMessage("FINEAMT promedio sin accidente: " + promedios3[0] + ", con accidente:" + promedios3[1]);
				break;


			case 4:

				view.printMessage("Ingrese el ADDRESS_ID");
				String addressId4 = sc.next();

				view.printMessage("Ingrese la fecha con hora inicial (Ej : 28/03/2017)");
				LocalDate fechaInicialReq4A = convertirFecha(sc.next());

				view.printMessage("Ingrese la fecha con hora final (Ej : 28/03/2017)");
				LocalDate fechaFinalReq4A = convertirFecha(sc.next());

				IStack<VOMovingViolations> resultados4 = controller.getMovingViolationsAtAddressInRange(addressId4, fechaInicialReq4A, fechaFinalReq4A);

				view.printMovingViolationsReq4(resultados4);

				break;

			case 5:
				view.printMessage("Ingrese el limite inferior de FINEAMT  (Ej: 50)");
				double limiteInf5 = sc.nextDouble();

				view.printMessage("Ingrese el limite superior de FINEAMT  (Ej: 50)");
				double limiteSup5 = sc.nextDouble();

				IQueue<VOViolationCode> violationCodes = controller.violationCodesByFineAmt(limiteInf5, limiteSup5);
				view.printViolationCodesReq5(violationCodes);
				break;

			case 6:

				view.printMessage("Ingrese el limite inferior de TOTALPAID (Ej: 200)");
				double limiteInf6 = sc.nextDouble();

				view.printMessage("Ingrese el limite superior de TOTALPAID (Ej: 200)");
				double limiteSup6 = sc.nextDouble();

				view.printMessage("Ordenar Ascendentmente: (Ej: true)");
				boolean ascendente6 = sc.nextBoolean();				

				IStack<VOMovingViolations> resultados6 = controller.getMovingViolationsByTotalPaid(limiteInf6, limiteSup6, ascendente6);
				view.printMovingViolationReq6(resultados6);
				break;

			case 7:

				view.printMessage("Ingrese la hora inicial (Ej: 23)");
				int horaInicial7 = sc.nextInt();

				view.printMessage("Ingrese la hora final (Ej: 23)");
				int horaFinal7 = sc.nextInt();

				IQueue<VOMovingViolations> resultados7 = controller.getMovingViolationsByHour(horaInicial7, horaFinal7);
				view.printMovingViolationsReq7(resultados7);
				break;

			case 8:

				view.printMessage("Ingrese el VIOLATIONCODE (Ej : T210)");
				String violationCode8 = sc.next();

				double [] resultado8 = controller.avgAndStdDevFineAmtOfMovingViolation(violationCode8);

				view.printMessage("FINEAMT promedio: " + resultado8[0] + ", desviación estandar:" + resultado8[1]);
				break;

			case 9:

				view.printMessage("Ingrese la hora inicial (Ej: 23)");
				int horaInicial9 = sc.nextInt();

				view.printMessage("Ingrese la hora final (Ej: 23)");
				int horaFinal9 = sc.nextInt();

				int resultado9 = controller.countMovingViolationsInHourRange(horaInicial9, horaFinal9);

				view.printMessage("Número de infracciones: " + resultado9);
				break;

			case 10:
				view.printMovingViolationsByHourReq10();
				break;

			case 11:
				view.printMessage("Ingrese la fecha inicial (Ej : 28/03/2018)");
				LocalDate fechaInicial11 = convertirFecha(sc.next());

				view.printMessage("Ingrese la fecha final (Ej : 28/03/2018)");
				LocalDate fechaFinal11 = convertirFecha(sc.next());

				double resultados11 = controller.totalDebt(fechaInicial11, fechaFinal11);
				view.printMessage("Deuda total "+ resultados11);
				break;

			case 12:	
				view.printTotalDebtbyMonthReq12(this);

				break;

			case 13:	
				fin=true;
				sc.close();
				break;
			}
		}

	}


	public void loadMovingViolations(int numeroCuatrimestre) {
		BufferedReader br;
		try {
			infraccionesQueue = new Queue<VOMovingViolations>();
			ArregloDinamico<VOMovingViolations> ar = new ArregloDinamico<VOMovingViolations>(150000);
			infraccionesStack = new Stack<VOMovingViolations>(ar);
			colaViolationCode = new  ArregloDinamico<VOViolationCode>(100000);
			infracciones = new ArregloDinamico<>(100000);
			String fecha1 = "January February March April";
			String fecha2 = "May June July August ";
			String fecha3 = "September October November December";
			String fecha=numeroCuatrimestre==1? fecha1:numeroCuatrimestre==2? fecha2:fecha3; 
			String nextLine= "";
			String[] nextLineS=null;
			if (fecha.equals(fecha3)) 
			{
				String[] fechas = fecha.split(" ");
				br = new BufferedReader(new FileReader("./data/Moving_Violations_Issued_in_" + fechas[0] +"_2018.csv"));
				nextLine = br.readLine();
				while ((nextLine = br.readLine()) != null) 
				{
					nextLineS= nextLine.split(",");
					String issuedat= nextLineS[13];
					String violationDesc= nextLineS[15];
					int objectID =Integer.parseInt(nextLineS[0].isEmpty()?"0":nextLineS[0]);
					double totalPaid = Double.parseDouble(nextLineS[9].isEmpty()?"0":nextLineS[9]);
					int addressID =	Integer.parseInt(nextLineS[3].isEmpty()?"0":nextLineS[3]);
					double penalty1 = Double.parseDouble(nextLineS[10].isEmpty()?"0":nextLineS[10]);
					double penalty2= Double.parseDouble(nextLineS[11].isEmpty()?"0":nextLineS[11]);
					int StreetsedID = Integer.parseInt(nextLineS[4].isEmpty()?"0":nextLineS[4]);
					String violatioCode =nextLineS[14];
					double fineamit = Double.parseDouble(nextLineS[8].isEmpty()?"0":nextLineS[8]);
					boolean accidente = nextLineS[12].equals("No")?false:true;

					VOMovingViolations infraccion = new VOMovingViolations( issuedat,violationDesc,
							objectID,
							totalPaid, 
							addressID,
							penalty1,
							penalty2,
							StreetsedID,violatioCode, fineamit,accidente);
					infraccionesQueue.enqueue(infraccion);
					infraccionesStack.push(infraccion);
					infracciones.agregar(infraccion);
					colaViolationCode.agregar(new VOViolationCode(violatioCode,fineamit, accidente));
				}


				nextLine= "";
				nextLineS=null;
				for (int i = 0; i < 3; i++) {
					br = new BufferedReader(new FileReader("./data/Moving_Violations_Issued_in_" + fechas[i+1] +"_2018.csv"));
					nextLine = br.readLine();
					while ((nextLine = br.readLine()) != null) 
					{
						nextLineS = nextLine.split(",");


						String issuedat= nextLineS[14];
						String violationDesc= nextLineS[16];
						int objectID=Integer.parseInt(nextLineS[0]);
						double totalPaid = Double.parseDouble(nextLineS[9].isEmpty()?"0":nextLineS[9]);
						int addressID =	Integer.parseInt(nextLineS[3].isEmpty()?"0":nextLineS[3]);
						double penalty1 = Double.parseDouble(nextLineS[10].isEmpty()?"0":nextLineS[10]);
						double penalty2= Double.parseDouble(nextLineS[11].isEmpty()?"0":nextLineS[11]);
						int StreetsedID = Integer.parseInt(nextLineS[4].isEmpty()?"0":nextLineS[4]);
						String violatioCode =nextLineS[15];
						double fineamit = Double.parseDouble(nextLineS[8].isEmpty()?"0":nextLineS[8]);
						boolean accidente = nextLineS[12].equals("No")?false:true;
						VOMovingViolations infraccion = new VOMovingViolations( issuedat,violationDesc,
								objectID,
								totalPaid, 
								addressID,
								penalty1,
								penalty2,
								StreetsedID,violatioCode, fineamit,accidente);
						infraccionesQueue.enqueue(infraccion);
						infraccionesStack.push(infraccion);
						infracciones.agregar(infraccion);
						colaViolationCode.agregar(new VOViolationCode(violatioCode,fineamit, accidente));
					}
				}
			}
			else{

				String[] fechas = fecha.split(" ");
				for (int j = 0; j < fechas.length; j++) {

					br = new BufferedReader(new FileReader("./data/Moving_Violations_Issued_in_" + fechas[j] +"_2018.csv"));
					nextLine = br.readLine();
					while ((nextLine = br.readLine()) != null) 
					{
						nextLineS = nextLine.split(",");
						String issuedat= nextLineS[13];
						String  violationDesc= nextLineS[15];
						int objectID=Integer.parseInt(nextLineS[0]);
						double totalPaid = Double.parseDouble(nextLineS[9].isEmpty()?"0":nextLineS[9]);
						int addressID =	Integer.parseInt(nextLineS[3].isEmpty()?"0":nextLineS[3]);
						double penalty1 = Double.parseDouble(nextLineS[10].isEmpty()?"0":nextLineS[10]);
						double penalty2= Double.parseDouble(nextLineS[11].isEmpty()?"0":nextLineS[11]);
						int StreetsedID = Integer.parseInt(nextLineS[4].isEmpty()?"0":nextLineS[4]);
						String violatioCode =nextLineS[14];
						double fineamit = Double.parseDouble(nextLineS[8].isEmpty()?"0":nextLineS[8]);
						boolean accidente = nextLineS[12].equals("No")?false:true;

						VOMovingViolations infraccion = new VOMovingViolations( issuedat,violationDesc,
								objectID,
								totalPaid, 
								addressID,
								penalty1,
								penalty2,
								StreetsedID,violatioCode, fineamit,accidente);
						infraccionesQueue.enqueue(infraccion);
						infraccionesStack.push(infraccion);
						infracciones.agregar(infraccion);
						colaViolationCode.agregar(new VOViolationCode(violatioCode,fineamit, accidente));
					}
				}
			}
			for (int i = 0; i < 10; i++) {
				VOMovingViolations a =infracciones.darElemento(i);
				System.out.println(a.toString() );
			}
		}
		catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}
	//requerimiento 1A
	public IQueue<VOMovingViolations> verifyObjectIDIsUnique() {
		IQueue<VOMovingViolations> copia = infraccionesQueue;
		IQueue<VOMovingViolations> repetidos = new Queue<VOMovingViolations>();
		while(!copia.isEmpty())
		{
			VOMovingViolations i1 = copia.dequeue();
			IQueue<VOMovingViolations> copia2 = copia;
			while(!copia2.isEmpty())
			{
				VOMovingViolations i2 = copia2.dequeue();
				if(i1.compareTo(i2) == 0)
					repetidos.enqueue(i2);
			}
		}
		return repetidos;
	}
	//requerimiento 2A
	public IQueue<VOMovingViolations> getMovingViolationsInRange(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		IQueue<VOMovingViolations> copia = infraccionesQueue;
		IQueue<VOMovingViolations> repetidos = new Queue<VOMovingViolations>();
		while(!copia.isEmpty())
		{
			VOMovingViolations i1 = copia.dequeue();
			LocalDateTime d1 = convertirFecha_Hora_LDT(i1.getTicketIssueDate());
			if(d1.isBefore(fechaFinal) && d1.isAfter(fechaInicial))
				repetidos.enqueue(i1);
		}
		return repetidos;
	}
	//requerimiento 3A
	//Sin accidente posicion 0 y con accidente posicion 1
	public double[] avgFineAmountByViolationCode(String violationCode3) {
		IArregloDinamico<VOViolationCode> copia = colaViolationCode;
		int cont1 = 0;
		int cont2 = 0;
		double prom1 = 0;
		double prom2 = 0;
		for(int i = 0; i < copia.darTamano(); i++)
		{
			VOViolationCode c = copia.darElemento(i);
			if(c.getViolationCode().equals(violationCode3))
			{
				if(c.getAccident())
				{
					prom2 += c.getAvgFineAmt();
					cont2++;
				}
				else
				{
					prom1 += c.getAvgFineAmt();
					cont1++;
				}
			}
		}
		if(cont1 != 0)
			prom1 = prom1/cont1;
		if(cont2 != 0)
			prom2 = prom2/cont2;
		return new double [] {prom1 , prom2};
	}
	//requerimiento 4A
	public IStack<VOMovingViolations> getMovingViolationsAtAddressInRange(String addressId, LocalDate fechaInicial, LocalDate fechaFinal) {
		IStack<VOMovingViolations> copia = infraccionesStack;
		VOMovingViolations[] a = new VOMovingViolations[150000];
		while(!copia.isEmpty())
		{
			VOMovingViolations i1 = copia.pop();
			LocalDate d1 = convertirFecha(i1.getTicketIssueDate());
			if(d1.isBefore(fechaFinal) && d1.isAfter(fechaInicial) && i1.getAdressId() == Integer.parseInt(addressId))
				a[a.length] = i1;
		}
		sort(a);
		ArregloDinamico<VOMovingViolations> ar = new ArregloDinamico<VOMovingViolations>(150000);
		for(int i = 0; i < a.length; i++)
		{
			ar.agregar(a[i]);
		}
		IStack<VOMovingViolations> resultados = new Stack<VOMovingViolations>(ar);
		return resultados;
	}
	private static void sort(VOMovingViolations[] a) {
		int n = a.length;
		int h = 1;
		while (h < n/3) h = 3*h + 1;
		while (h >= 1) {
			for (int i = h; i < n; i++) {
				for (int j = i; j >= h && less(a[j], a[j-h]); j -= h) {
					exch(a, j, j-h);
				}
			} 
			h /= 3;
		}
	}
	private static boolean less(VOMovingViolations v, VOMovingViolations w) {
		return v.getStreetSegid() < w.getStreetSegid()?true:(v.getStreetSegid() == w.getStreetSegid()?(convertirFecha_Hora_LDT(v.getTicketIssueDate()).isBefore(convertirFecha_Hora_LDT(w.getTicketIssueDate()))):false);
	}
	private static void exch(Object[] a, int i, int j) {
		Object swap = a[i];
		a[i] = a[j];
		a[j] = swap;
	}


	public IQueue<VOViolationCode> violationCodesByFineAmt(double limiteInf5, double limiteSup5)   {
		Queue<VOViolationCode> cola1 = new Queue<VOViolationCode>();
		for (int i = 0; i < colaViolationCode.darTamano(); i++) {
			VOViolationCode vcode= colaViolationCode.darElemento(i);
			if(vcode.getAvgFineAmt()>limiteInf5 && vcode.getAvgFineAmt()<=limiteSup5)
			{
				cola1.enqueue(vcode);
			}	
		}

		return cola1;
	}


	public IStack<VOMovingViolations> getMovingViolationsByTotalPaid(double limiteInf6, double limiteSup6,
			boolean ascendente6) {
		Stack<VOMovingViolations> resultado  = new Stack<>(new ArregloDinamico<VOMovingViolations>(20000));
		VOMovingViolations[] copy =new VOMovingViolations[150000];
		int i=0;
			for (VOMovingViolations voMovingViolations : infraccionesQueue) {
				
				if(voMovingViolations.getTotalPaid()>limiteInf6&& voMovingViolations.getTotalPaid()<limiteSup6)
				{
					copy[i]=voMovingViolations;
					System.out.println(voMovingViolations);
					i++;
				}
			
		}

		 return sortReq6(copy, ascendente6);
		
	}
	private static Stack<VOMovingViolations> sortReq6(VOMovingViolations[] s, boolean orden)
	{
		Stack<VOMovingViolations> x = new Stack<VOMovingViolations>(new ArregloDinamico<>(150000));
		if (orden== true)
		{
			
			int n = s.length;
			int h = 1;
			while (h < n/3) h = 3*h + 1;
			while (h >= 1) {
				for (int i = h; i < n; i++) {
					System.out.println(s[0].getTicketIssueDate());
					for (int j = i; j >= h && les1((VOMovingViolations)s[j], (VOMovingViolations)s[j-h]); j -= h) {
						
						exch(s, j, j-h);
					}
				} 
				h /= 3;
			}
			
			for(int i=0;i<s.length;i++)
			{
				x.push(s[i]);
			}
		}
		return x ;
	
	}
	private static boolean les1(VOMovingViolations v, VOMovingViolations w) {
		System.out.println(v);
		return v.getTicketIssueDate().compareTo( w.getTicketIssueDate())<0?true: false;
	
	
	}

	public IQueue<VOMovingViolations> getMovingViolationsByHour(int horaInicial7, int horaFinal7) {
		// TODO Auto-generated method stub
		return null;
	}

	public double[] avgAndStdDevFineAmtOfMovingViolation(String violationCode8) {
		// TODO Auto-generated method stub
		return new double [] {0.0 , 0.0};
	}

	public int countMovingViolationsInHourRange(int horaInicial9, int horaFinal9) {
		// TODO Auto-generated method stub
		return 0;
	}

	public double totalDebt(LocalDate fechaInicial11, LocalDate fechaFinal11) {
		double total = 0;
		IQueue<VOMovingViolations> copia = infraccionesQueue;
		while(!copia.isEmpty())
		{
			VOMovingViolations i1 = copia.dequeue();
			LocalDate d1 = convertirFecha(i1.getTicketIssueDate());
			if(d1.isBefore(fechaFinal11) && d1.isAfter(fechaInicial11))
				total += (i1.getFineAMT()+i1.getPenalty1()+i1.getPenalty2()-i1.getTotalPaid());
		}
		return total;
	}
	public double[] deudaPorMes()
	{
		LocalDate di = LocalDate.parse("01/01/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		LocalDate df = LocalDate.parse("31/01/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		double debt = totalDebt(di, df);
		double[] resultado = new double[4];
		if(debt > 0)
		{
			resultado[0] = debt;
			di = LocalDate.parse("01/02/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			df = LocalDate.parse("28/02/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			debt += totalDebt(di, df);
			resultado[1] = debt;
			di = LocalDate.parse("01/03/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			df = LocalDate.parse("31/03/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			debt += totalDebt(di, df);
			resultado[2] = debt;
			di = LocalDate.parse("01/04/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			df = LocalDate.parse("30/04/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			debt += totalDebt(di, df);
			resultado[3] = debt;
		}
		else
		{
			di = LocalDate.parse("01/05/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			df = LocalDate.parse("31/05/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			debt = totalDebt(di, df);
			if(debt > 0)
			{
				resultado[0] = debt;
				di = LocalDate.parse("01/06/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				df = LocalDate.parse("30/06/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				debt += totalDebt(di, df);
				resultado[1] = debt;
				di = LocalDate.parse("01/07/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				df = LocalDate.parse("31/07/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				debt += totalDebt(di, df);
				resultado[2] = debt;
				di = LocalDate.parse("01/08/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				df = LocalDate.parse("31/08/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				debt += totalDebt(di, df);
				resultado[3] = debt;
			}
			else
			{
				di = LocalDate.parse("01/09/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				df = LocalDate.parse("30/09/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				debt += totalDebt(di, df);
				resultado[0] = debt;
				di = LocalDate.parse("01/10/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				df = LocalDate.parse("31/10/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				debt += totalDebt(di, df);
				resultado[1] = debt;
				di = LocalDate.parse("01/11/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				df = LocalDate.parse("30/11/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				debt += totalDebt(di, df);
				resultado[2] = debt;
				di = LocalDate.parse("01/12/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				df = LocalDate.parse("31/12/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				debt += totalDebt(di, df);
				resultado[3] = debt;
			}
		}
		if(resultado.length == 0)
			resultado = new double [] {0 , 0, 0};
		return resultado;
	}

	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		return LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	}


	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora)

	{

		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));

	}
}
